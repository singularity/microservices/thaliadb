## Thaliadb container

### use:
- Apache2
- PostgreSQL
- MongoDB
- Django


### build sif container
```bash
sudo singularity build thaliadb.3.5.3.sif Singularity.thaliadb.3.5.3.def
```

#### Start instance using permanant overlay:
```bash
mkdir -p overlay_thalia353/
sudo singularity instance start --overlay overlay_thalia353/ thaliadb.3.5.3.sif thalia353
```

#### Stop instance:
```bash
sudo singularity instance stop thalia353
```

#### Modification innside the container:
```bash
sudo singularity shell --writable instance://thalia353
```

