import os
import sys

"""
    CUSTOM SETTINGS
"""

ROOT_URL = '/'
#Apache settings

DJANGO_ROOT_SETTING = ''
#Database settings

DATABASES = {
    'default': {

        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'HOST':'localhost',
        'PORT':'5439',
        'NAME': 'thalia',
#         'NAME': 'test-template',
        'USER': 'thaliadbadmin',
        'PASSWORD': 'db',
        'ATOMIC_REQUESTS': True,
    },
}

MONGODB_DATABASES = {
    "default": {
        "name": "genotyping",
        "host": '127.0.0.1',
        "port":27018,
        #"password": '',
        #"username": '',
        #"tz_aware": True, # if you using timezones in django (USE_TZ = True)
        
    },
}

# Path to store heavy files
HEAVYFILES_URL = '/tmp/'

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/Paris'

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = '/var/www/django-apps/thalia/media/'

# Used by Django File Form module 
DEFAULT_FILE_STORAGE = MEDIA_ROOT

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['localhost','127.0.0.1','pac-sm-gafl01' , 'pac-sm-gafl01.avignon.inra.fr', '147.100.168.2', '[::1]']

TEMP_FOLDER = "" #utiliser le module tmpfile de python

