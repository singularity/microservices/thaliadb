#!/bin/bash

apache_port=8087

host=localhost

pg_port=5439
pg_db=
pg_user=
pg_password=

mongo_port=
mongo_db=genotyping
mongo_user=thaliadbadmin
mongo_password=db

###### db_user_create.js
sed -e 's/MONGOUSER/'$mongo_user'/g' \
-e 's/MONGOPASSWORD/'$mongo_password'/g' \
-e 's/MONGODBNAME/'$mongo_db'/g' template_db_user_create.js >db_user_create.js

##### django-apps.conf
sed -e 's/APACHEPORT/'$apache_port'/g' template_django-apps.conf >django-apps.conf



exit 0

